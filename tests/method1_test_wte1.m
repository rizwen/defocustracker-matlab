%% Test METHOD 1: WTE1

%  initialize
clc, clear, close all

% set path with data of WTE1
path_wte1 = 'C:\Users\Massi\Documents\Programming\MATLAB\DefocusTracker-Matlab\';

%  create the imageset for the training images
D = dir([path_wte1,'\Datasets-WTE1\Calib-noise0\*.tif']);
folder_name = D.folder;
image_names = {D(:).name};
training_images = dtracker_create('imageset',folder_name,image_names);

%  create the imageset for the experimental images
D = dir([path_wte1,'.\Datasets-WTE1\Data-overlapping-noise0-particles1100\*.tif']);
folder_name = D.folder;
image_names = {D(:).name};
noise0_p500_images = dtracker_create('imageset',folder_name,image_names);

%  create the training data
selected_frames = 1:10:501;
N_cal = length(selected_frames);
training_data = dtracker_create('dataset',N_cal);
training_data.x = zeros(1,N_cal) + 30;
training_data.y = zeros(1,N_cal) + 30; 
training_data.z = linspace(0,1,N_cal);
training_data.fr = selected_frames;

%  Create the model
model = dtracker_create('model','method_1');
model.training.imwidth = 51;
model.training.imheight = 51;
model = dtracker_train(model,training_images,training_data);

%  Run the evaluation 
model.process.cm_guess = .4;
model.process.cm_final = .5;
N_frames = noise0_p500_images.n_frames;
noise0_p500_data = dtracker_process(model, noise0_p500_images, 1:N_frames);

%  Run the error analysis
load([path_wte1,'\Datasets-WTE1\true_coordinates']);
par.outliers_x = 2;
par.outliers_y = 2;
par.outliers_z = 0.1;
par.delta_points = 20;
[errors, errors_delta] = dtracker_postprocess('compare_true_values',...
   noise0_p500_data,noise0_p500_true,par);

%  Final plots
dtracker_show(model)
dtracker_show(noise0_p500_images,noise0_p500_data)

figure
subplot(3,1,1)
plot(errors_delta.z,errors_delta.sigma_z,'.-')
xlabel('z/h'), ylabel('\sigma^{\delta}_z/h')
title(['\sigma_z/h = ',num2str(errors.sigma_z,'%0.3f')])
subplot(3,1,2)
plot(errors_delta.z,errors_delta.sigma_x,'.-',errors_delta.z,errors_delta.sigma_y,'.-')
xlabel('z/h'), ylabel('\sigma^{\delta}_{x,y} (px)')
title(['\sigma_{x,y} = ',num2str((errors.sigma_x+errors.sigma_y)/2,'%0.3f'),' (px)'])
legend('\sigma^{\delta}_x','\sigma^{\delta}_y')
subplot(3,1,3)
plot(errors_delta.z,errors_delta.recall,'.-')
xlabel('z/h'), ylabel('recall^{\delta}')
title(['recall = ',num2str(errors.recall,'%0.3f')])


disp('          ')
disp('------ Error analysis ------')
disp(['Error for x (pixels) = ',num2str(errors.sigma_x,'%0.3f')])
disp(['Error for y (pixels) = ',num2str(errors.sigma_y,'%0.3f')])
disp(['Error for z/h = ',num2str(errors.sigma_z,'%0.3f')])
disp(['Recall = ',num2str(errors.recall,'%0.3f')])
disp('------ Expected values ------')
disp('Error for x (pixels) = 0.439')
disp('Error for y (pixels) = 0.442')
disp('Error for z/h = 0.023')
disp('Recall = 0.331')

