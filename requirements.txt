DefocusTracker has been tested on Matlab R2018b or higher. 

The following toolboxes are required:
- curve_fitting_toolbox
- image_toolbox
- statistics_toolbox