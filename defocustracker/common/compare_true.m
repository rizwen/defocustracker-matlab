function [errors, varargout] = compare_true(dat_meas,dat_true,par)


%%
N_meas = max(dat_meas.fr);
N_true = max(dat_true.fr);

if N_meas~=N_true
    disp('Error: Frames number does not match')
    return
end

dat_meas.fr = dat_meas.fr*2-1;
dat_true.fr = dat_true.fr*2;
dat_meas = dtracker_postprocess('unscale',dat_meas);
dat_true = dtracker_postprocess('unscale',dat_true);
dat = dtracker_postprocess('merge',dat_meas,dat_true);

frame_index = 1:2:2*N_true-1;
tracking_step = 1;
bounding_box = [[-1 1]*par.outliers_x, ...
    [-1 1]*par.outliers_y, ...
    [-1 1]*par.outliers_z];

dat = ptv_nearest(dat,frame_index,tracking_step,bounding_box);

%% calculate error
flag_tp = dat.id~=0 & mod(dat.fr,2)==1;
flag_fn = dat.id==0 & mod(dat.fr,2)==0;
tp = sum(flag_tp);
fn = sum(flag_fn);

dx = dat.dx(flag_tp);
dy = dat.dy(flag_tp);
dz = dat.dz(flag_tp);

errors.sigma_x = sqrt(1/tp*sum(dx.^2));
errors.sigma_y = sqrt(1/tp*sum(dy.^2));
errors.sigma_z = sqrt(1/tp*sum(dz.^2));
errors.recall = tp/(tp+fn);


%% calculate error_delta

if nargout==2
    
    z_ref = dat.z(flag_tp)+dat.dz(flag_tp);
    z_ref_all = [z_ref, dat.z(flag_fn)];
    z = linspace(0,1,par.delta_points);
    delta_z = z(2)-z(1);
    
    errors_delta.z = z;
    errors_delta.sigma_x = z*0;
    errors_delta.sigma_y = z*0;
    errors_delta.sigma_z = z*0;
    errors_delta.recall = z*0; errors_delta.dat = dat;
    
    for ii = 1:length(z)
        flag_tp_z = abs(z_ref-z(ii))<=delta_z;
        flag_fn_z = abs(z_ref_all-z(ii))<=delta_z;
        
        errors_delta.sigma_x(ii) = sqrt(1/sum(flag_tp_z)*sum((dx(flag_tp_z)).^2));
        errors_delta.sigma_y(ii) = sqrt(1/sum(flag_tp_z)*sum((dy(flag_tp_z)).^2));
        errors_delta.sigma_z(ii) = sqrt(1/sum(flag_tp_z)*sum((dz(flag_tp_z)).^2));       
        errors_delta.recall(ii) = sum(flag_tp_z)/sum(flag_fn_z);
    end
    
    varargout{1} = errors_delta;
end