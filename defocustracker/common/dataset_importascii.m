function dat = dataset_importascii(name,varargin)

A = importdata(name);
data = A.data';
variables = A.colheaders;
if nargin==2
    if strcmp(varargin{1},'struct')
        for ii = 1:length(variables)
            eval(['dat.',variables{ii},' = data(ii,:);'])
        end
    end
else
    dat = dataset_create(size(data,2));
    for ii = 1:length(variables)
        if isfield(dat,variables{ii})
            eval(['dat.',variables{ii},' = data(ii,:);'])
        end
    end
    dat.scaling = dat.scaling(1:4);
end