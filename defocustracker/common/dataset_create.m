function this = dataset_create(varargin)

prop = {'x','y','z','dx','dy','dz','fr','id','cm'};

if nargin==9
    for ii = 1:length(prop)
        eval(['this.',prop{ii},' = varargin{',num2str(ii),'};'])
    end
elseif nargin==1
    data = zeros(1,varargin{1});
    for ii = 1:length(prop)
        eval(['this.',prop{ii},' = data;'])
    end
else
    for ii = 1:length(prop)
        eval(['this.',prop{ii},' = [];'])
    end
end

this.scaling = [1 1 1 0];
this.type = 'dataset';
this.metadata.images_boundary = [];
this.metadata.tracking_step = 0;

