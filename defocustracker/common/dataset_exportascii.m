function dataset_exportascii(dat,name,varargin)

if nargin==3
    variables = varargin{1};
else
    variables = fields(dat);
end

N = zeros(1,length(variables));
for ii = 1:length(variables)
    N(ii) = eval(['length(dat.',variables{ii},')']);
end
N = max(N);

txtdat = zeros(length(variables),N);
wrong_input = false(length(variables),1);
txtformat = [];
fileID = fopen(name,'wt');

for ii = 1:length(variables)
    var = eval(['dat.',variables{ii}]); 
    if ~isnumeric(var) || size(var,1)~=1
        wrong_input(ii) = 1;
        continue
    end   
    txtdat(ii,1:length(var)) = var;
    txtformat = [txtformat, '%12.6f '];
    fprintf(fileID,'%12s ',variables{ii});
end

txtformat = [txtformat(1:end-1),'\n'];
txtdat(wrong_input,:) = [];

fprintf(fileID,'%s\n',' ');
fprintf(fileID,txtformat,txtdat);
fclose(fileID);