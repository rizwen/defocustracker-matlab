function varargout = dataset_show(dat,this,varargin)

if nargin==2
    dat_name = inputname(1);
    img_name = inputname(2);
elseif nargin==4
    dat_name = varargin{1};
    img_name = varargin{2};
end

[f, ax] = imageset_show(this);
set(f,'name',['dataset: ',dat_name,' / imageset: ',img_name],'numbertitle','off')
this = getappdata(f,'this');

if isfield(dat,'metadata')
    if isfield(dat.metadata,'tracking_step')
        this.tracking_step = dat.metadata.tracking_step;
    else
        this.tracking_step = 0;
    end
    if isfield(dat.metadata,'images_boundary')
        this.images_boundary = dat.metadata.images_boundary;
    else
        this.images_boundary = [];
    end
end
this.double_frame = 1;
this.dat = dtracker_postprocess('unscale',dat);
this.old_update = this.handles.bnr.Callback{1};
this.handles.bnr.Callback = {@update_nr,f};


hold on
this.partc = plot(1,1,'+');
this.part = plot(1,1,'g');
this.vect = quiver(1,1,1,1,0);

if this.tracking_step>0
    btt = uicontrol('Parent',f,'Style','togglebutton',...
    'units','normalized','position',[.33 .015 .05 .04],'String','1-2');
    btt.Callback = {@next_btt,f,this.tracking_step};
    
    uicontrol('Parent',f,'Style','text','units','normalized',...
    'position',[.45 .045 .1 .04],'String','Vector scale',...
    'horizontalalignment','center')
    this.handles.vect_scale = uicontrol('Parent',f,'Style','edit','units','normalized',...
    'position',[.45 .015 .1 .04],'String',num2str(1));
    this.handles.vect_scale.Callback = {@update_nr,f};
    
end
 
ind = find(dat.id~=0,1,'first');
if isempty(ind), ind=1; end
set(this.handles.bnr,'String',dat.fr(ind))
setappdata(f,'this',this);
update_nr([],[],f)

if nargout==1
    varargout{1} = f;
elseif nargout==2
    varargout{1} = f;
    varargout{2} = ax;
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function update_nr(~,~,f)
this = getappdata(f,'this');
this.old_update([],[],f)

fr = round(str2double(this.handles.bnr.String));

flag = this.dat.fr==fr;
xp = this.dat.x(flag);
yp = this.dat.y(flag);
zp = this.dat.z(flag);

[xf, yf] = give_positions(xp,yp,zp,this.images_boundary);
set(this.partc,'Xdata',xp,'YData',yp)
set(this.part,'Xdata',xf,'YData',yf)
if ~isempty(zp) && this.tracking_step>0
    vector_scale = round(str2double(this.handles.vect_scale.String));
    dxp = this.dat.dx(flag)*vector_scale;
    dyp = this.dat.dy(flag)*vector_scale;
    set(this.vect,'Xdata',xp,'YData',yp,'UData',dxp,'VData',dyp)
else
    set(this.vect,'Xdata',0,'YData',0,'UData',0,'VData',0)
end
end

function next_btt(~,~,f,skn)
this = getappdata(f,'this');
n = round(str2double(this.handles.bnr.String))+skn;
if n<=this.n_frames
    if this.double_frame==1
        im = imageset_read(this.img,n,'raw');
        set(this.him,'CData',im);
        this.double_frame = 2;
    else
        im = imageset_read(this.img,n-1,'raw');
        set(this.him,'CData',im);
        this.double_frame = 1;
    end
end
setappdata(f,'this',this);
end

function [xf, yf] = give_positions(x,y,z,imb)
if isempty(imb), xf = []; yf = []; return, end
z_imb = linspace(0,1,length(imb));
xf = [];
yf = [];
for ii = 1:length(x)
    [~, ind] = min(abs(z_imb-z(ii)));
    xf = [xf, NaN, x(ii)+imb{ind}(1,:)];
    yf = [yf, NaN, y(ii)+imb{ind}(2,:)];
end
end
    

% keyboard