function out = dtracker_create(what_to_create,varargin)

switch what_to_create
    case 'dataset'
        if nargin == 10
            out = dataset_create(varargin{1},varargin{2},varargin{3},...
                varargin{4},varargin{5},varargin{6},...
                varargin{7},varargin{8},varargin{9});
        elseif nargin==2
            out = dataset_create(varargin{1});
        else
            out = dataset_create;
        end
    case 'imageset'
        if nargin == 3
            out = imageset_create(varargin{1},varargin{2});
        else
            out = imageset_create;
        end
        
    case 'model'
        switch varargin{1}
            case 'method_1'   
                out = calibration_1_create();
        end
        
    case 'import_ascii'
        if nargin==2
            name = varargin{1};
            out = dataset_importascii(name);
        elseif nargin==3
            name = varargin{1};
            out = dataset_importascii(name,varargin{2});
        end
    
    case 'export_ascii'
        dat = varargin{1};
        name = varargin{2};
        if nargin==3
            dataset_exportascii(dat,name)
        elseif nargin==4
            variables = varargin{3};
            dataset_exportascii(dat,name,variables)
        end
        
    case 'scaling_xy'
        if nargin == 1
            scaling_xy_create
        elseif nargin == 2
            scaling_xy_create(varargin{2})
        end
        
    otherwise
        return
end



