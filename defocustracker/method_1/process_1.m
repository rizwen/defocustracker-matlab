function [dat, process_time] = process_1(imageset,cal,frame_index,par)

disp('Evaluation started ...')

dat = dtracker_create('dataset');
eval_times = frame_index*0;

ind = find(frame_index<=imageset.n_frames,1,'last');
frame_index = frame_index(1:ind);

n_frames = length(frame_index);

for n = 1:n_frames
    im = imageset_read(imageset,(frame_index(n)));
    
    [datd, eval_times(n)] = singleprocess_1(im,cal,par);
    datd.fr(:) = frame_index(n);
    dat = dtracker_postprocess('merge',dat,datd);
    timestr = secs2hms(eval_times(n)*(n_frames-n));
    if n==n_frames, timestr = ' '; end
    
    note_eval = [num2str(n),'/',num2str(n_frames),' - ',...
        num2str(round(eval_times(n))),' s - ',...
        num2str(length(datd.x)),' particles - time left: ',...
        timestr];
    
    disp(note_eval)
end

if par.tracking_step>0
    bounding_box = [par.minmax_dx, par.minmax_dy, par.minmax_dz];
    dat = ptv_nearest(dat,frame_index,par.tracking_step,bounding_box);
end


dat.scaling = [par.scaling_xyz(1:3), 0];
dat.metadata.images_boundary = cal.images_boundary;
dat.metadata.tracking_step = par.tracking_step;
% dat = dtracker_postprocess('scale',dat);

process_time = sum(eval_times);
timestr = secs2hms(process_time);
disp('----------------------------')
disp(['Evaluation done! Total Time: ',timestr])





