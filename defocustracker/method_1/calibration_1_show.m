function varargout = calibration_1_show(calib,varargin)

if nargin==1
    name_var = inputname(1);
elseif nargin==2
    name_var = varargin{1};
end

cal = calib.parameter;
cal.imresize = calib.training.imresize;

xy0 = (size(cal.images{1})+1)/2;                    % define origo
f = figure; set(f,'position',[100 50 900 600])  % initialize figure
set(f,'name',['model: ',name_var],'numbertitle','off')

% Plot: Signal-to-noise ratio vs. image number
axes('Parent',f,'position',[.08 .61 .38 .36]);
plot(cal.z,cal.signal_to_noise,'.-');
xlim([0 1]) 
xlabel('z/h'); ylabel('Signal-to-noise ratio');


% Plot: Similarity rate of change vs. image number
axes('Parent',f,'position',[.6 .61 .38 .36]);
s_neighbor = diag(cal.similarity_map,-1)';
z_neighbor = cal.z(1:end-1)+diff(cal.z(1:2))/2;
plot(z_neighbor,s_neighbor,'.-');
xlim([0 1]) 
xlabel('z/h'); ylabel('Similarity');

% Plot: Correlation matrix surface
axes('Parent',f,'position',[.6 .1 .35 .35]);
if numel(cal.similarity_map)>1
    surf(cal.similarity_map)
else
    plot3(1,1,cal.similarity_map)
end
xlabel('j'), ylabel('i'), zlabel('Cm')
xlim([0, size(cal.similarity_map,2)+1]), ylim([0, size(cal.similarity_map,1)+1]), zlim([0 1])

% Plot: Particle image slider
axf = axes('Parent',f,'position',[.08 .16 .37 .34]);
% axes('Parent',f,'position',[.08 .61 .38 .36]);
% him = imagesc(cal.images{1}); 
him = imshow(cal.images{1},...
    [min(cal.images{1}(:)) max(cal.images{1}(:))+1],...
    'initialMagnification','fit'); 
hold on, daspect([1 1 1])
size_c = size(cal.images{1});
plot((size_c(2)+1)/2,(size_c(1)+1)/2,'+')
hpl = plot(cal.images_boundary{1}(1,:)*cal.imresize+xy0(2),cal.images_boundary{1}(2,:)*cal.imresize+xy0(1),'g');
set(gca,'YDir','normal',...
    'XTick',[1,(size_c(2)+1)/2,size_c(2)],...
    'YTick',[1,(size_c(1)+1)/2,size_c(1)])
xlabel('X'), ylabel('Y')
bl1 = uicontrol('Parent',f,'Style','text','units','normalized',...
    'position',[.43 .015 .1 .04],'String',['1/',num2str(length(cal.images))],'Fontsize',11);
if length(cal.images)>1
    b = uicontrol('Parent',f,'Style','slider','units','normalized','SliderStep',[1 1]/(length(cal.images)-1),...
        'position',[.1 .02 .33 .04],'Value',1,'min',1,'max',length(cal.images));
    b.Callback = {@updateSlider,him,hpl,xy0,cal.images_boundary,cal.images,bl1,axf,cal.imresize};
end

if nargout==1
    varargout{1} = s2;
end

function updateSlider(a,~,him,hpl,xy0,image_boundary,im_c,bl1,axf,imr)

n = round(a.Value);
if n==0, n=1; end
set(axf,'CLim',[min(im_c{n}(:)) max(im_c{n}(:)+1)])
set(him,'CData',im_c{n})
set(hpl,'XData',image_boundary{n}(1,:)*imr+xy0(2),...
    'YData',image_boundary{n}(2,:)*imr+xy0(1))
set(bl1,'String',[num2str(n),'/',num2str(length(im_c))])
