function [dat, varargout] = dtracker_process(model,imageset,frame_index)

switch model.method
    case 'method_1'
        cal = merge_struct(model.training,model.parameter);
        par = model.processing;
        [dat, process_time] = process_1(imageset,cal,frame_index,par);
        if nargout==2
            varargout{1} = process_time;
        end
        dat = dtracker_postprocess('scale',dat);
end


function s = merge_struct(s1,s2)
s = s1;
f = fieldnames(s2);
for ii = 1:length(f)
    s.(f{ii}) = s2.(f{ii});
end