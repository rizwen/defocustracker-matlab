function varargout = dtracker_show(var,varargin)

name_var = inputname(1);
switch var.type
    case 'imageset'
        if nargin==1
            imageset_show(var,name_var)
        elseif nargin==2
            if isnumeric(varargin{1})
                im = imageset_read(var,varargin{1});
                varargout{1} = im;
            elseif isstruct(varargin{1})
                name_var2 = inputname(2);
                dataset_show(varargin{1},var,name_var2,name_var)
            end
        end
    case 'dataset'
        if nargin==1
            var = dtracker_postprocess('unscale',var);
            dataset_validate(var,name_var)
        elseif nargin==2
            if strcmp('scaled',varargin{1})
                var = dtracker_postprocess('scale',var);
                dataset_validate(var,name_var)   
            elseif strcmp('plot_3d',varargin{1})
                figure
                plot3(var.x,var.y,var.z,'.'), grid on
                xlabel('x'), ylabel('y'), zlabel('z')
            else
                name_var2 = inputname(2);
                dataset_show(var,varargin{1},name_var,name_var2)
            end
        end
        
    case 'model'
        switch var.method
            case 'method_1'   
                calibration_1_show(var,name_var)
        end

end
