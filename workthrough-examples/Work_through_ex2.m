%% DefocusTracker - Work Through Example 2 (WTE2)
%  3D tracking of particles inside an evaporating droplet.
%  www.defocustracking.com
%
%  Introduction
%  ------------
% 
%  DefocusTracker is a software for the 3D localization and tracking of the
%  positions of particles via their defocused particle images using the
%  General Defocusing Particle Tracking (GDPT) method.
%
%  You can find more information about DefocusTracker and GDPT on
%  https://defocustracking.com/
%
%  DefocusTracker is based on three data structures (MATLAB struct):  
%      IMAGESET, DATASET, MODEL,
%  and on five main functions: 
%      dtracker_create(), dtracker_show(), dtracker_train(),  
%      dtracker_process(), dtracker_postprocess(). 
%  You will familiarize with these elements throughout this tutorial.
%
%  This second example is based on experimental data, adapted from 
%  Rossi et al. (2019) Physical Review E, 100(3), 033103. In this tutorial 
%  you will learn to apply DefocusTracker to measure flows in real 
%  applications. You will also learn to apply advanced method to optimize 
%  your measurement and correct for bias error. At the end of this tutorial
%  you will be able to apply DefocusTracker in challenging engineering and
%  physical applications.
%
%  WARNING: Be sure to complete and understand the WTE1 before proceeding 
%           with this tutorial!
%
%  Instruction
%  ------------
%
%  - Install DefocusTracker (see the README.md file)
%  - Complete WTE1 (see Work_through_ex1.m)
%  - Download the 'Datasets-WTE2' from https://defocustracking.com/
%  - Follow the instructions and run this script cell-by-cell using the
%    command (Ctrl+Enter).
% 

%% Initialization
clc, clear, close all
%  Insert here the path where you copied the folder 'Datasets-WTE2'
%  If you copied it in the same folder where the 'Work_through_ex2.m' is,
%  you can use '.\Datasets-WTE2'.
path_wte2 = '.\Datasets-WTE2';

%% =========== Step 1: Prepare the calibration MODEL =============

%  Create a default MODEL using method_1
model = dtracker_create('model','method_1');

%  Create the training IMAGESET. The training images are typically taken by 
%  scanning along z tracer particles at fixed location (e.g. particle 
%  sedimented on the bottom of a microchannel)
D = dir(fullfile(path_wte2,'Scan-step2um','*.tif'));
folder_name = D.folder;
image_names = {D(:).name};
images_train = dtracker_create('imageset',folder_name,image_names);
N_cal = images_train.n_frames;

%  Create a training DATASET: For method_1, the training set consists of 
%  the coordinate of a single particle (one per frame) at known xyz 
%  coordinates. 
data_train = dtracker_create('dataset',N_cal);
data_train.fr = 1:N_cal;
data_train.x = zeros(1,N_cal) + 350;
data_train.y = zeros(1,N_cal) + 588;
data_train.z = linspace(0,1,N_cal);

%  Set up the training parameters of the MODEL
model.training.imwidth = 45;            % width of the particle image
model.training.imheight = 39;           % height of the particle image
model.training.median_filter = 3;       % median filter to reduce thermal noise
model.training.smoothing = 0.2;         % smoothing (optional)
model.training.n_interp_images = 100;   % interpolation (optional)
% 
%  In comparison with WTE1 we used here more advanced features with
%  smoothing and interpolation: we smoothed the calibration images and
%  interpolate them to obtain 100 images from 51. You can try to vary
%  these parameters (smoothing typically between 0.1 and 0.2,
%  n_inter_images typically no more than 100) to improve the results in
%  noisy data.

%  Train the MODEL
model = dtracker_train(model,images_train,data_train); 

%  Inspect the MODEL
dtracker_show(model)

%  TIP: You can use a GUI panel to train the model on an imageset when 
%  using with method_1. You need first to create a model, and then run the 
%  dtracker_train() function without a dataset:
%  >> dtracker_train(model,images_train)

%% =========== Step 2: Find the scaling factor =============
%  A DefocusTracker evaluation gives results in unscaled units, which 
%  are pixels for {x,y,dx,dy} and depth-normalized unit for {z,dz}.
%  To obtain values in real units, one must obtain the scaling factor for
%  x,y,z.
%
%  SCALING FACTOR for x and y
%  In the current example, we use �m as units, so the scaling factor
%  must be in �m/pixels. For instance, if the pixel size is 6.5 µm and the
%  lens magnification is 5x, the scale factor would be approximately 
%  6.5/5 = 1.3 µm/pixels.
%  In the following example, we also used a cylindrical lens in the optics
%  (see more details in Barnkob & Rossi, Exp Fluids 61, 110 (2020)], 
%  therefore we have different scaling factors for x and y. 

%  Experimentally, the scaling factor is obtained by taking an image of a
%  calibration grid or a reference object of known dimension. In this
%  example we used a calibration grid with a 100 µm pitch. The image of the
%  calibration grid is stored in the folder: '.\Grid-100um'. 
%  The values are:
scaling_x = 1.2659; % �m/pixels
scaling_y = 1.1965; % �m/pixels 
%
%  EXERCISE: DefocusTracker has a GUI to calculate the scaling factor from 
%  images of squared grid. You can try it out running the command:
%  >> dtracker_create('scaling_xy')
%  --> select the image .\Grid-100um\B00001.tif
%  --> push 'select cross'
%  --> select a square around a cross, double click on the rectangle to
%      confirm
%  --> Insert the Pitch value (100 um and 100 um)
%  --> push 'calculate grid'
%  --> push 'take values'. The calculated scaling should appear in your
%      Workspace

%  SCALING FACTOR for z
%  The unscaled z values are normalized with the depth of the measurement
%  volume, and range between 0 and 1. The z scaling factor is given by the
%  depth of the measurement volume in physical units. For the present case,
%  the training images are taken with steps of 2 um, therefore the total
%  height can be obtained by the total number of images - 1 times 2 um.
%  Two IMPORTANT factors must also be taken into account:
%  1.  A MINUS SIGN must be multiplied to the scaling factor.
%      The scanning procedure is taken with an inverted microscope moving
%      the objective upwards: particles in the first frame are farther from
%      the lens, particles in the last frame are closer to the lens.
%      During a measurement it is the other way around: therefore a minus
%      sign must be multiplied to the scaling factor. 
%  2.  The fluid REFRACTIVE INDEX must be multiplied to the scaling factor.
%      This aspect is a bit more complex and it is also an approximation.
%      It depends to the fact that during the calibration, the objective is
%      moving in air, whereas during measurement the objective is fixed and
%      the particles are moving in water. We will not discuss in detail
%      this aspect, but for most practical applications in microfluidics
%      this is a good approximation.
refractive_index = 1.33;
step_length = 2; % (�m)
scaling_z = - step_length*(N_cal-1)*refractive_index;

%  SCALING FACTOR for dx, dy, and dz.
%  DefocusTracker calculates the DISPLACEMENT, NOT THE VELOCITY. To obtain
%  the velocity, the values of dx, dy, and dz must be divided by the time
%  interval between two frams. In this example (very slow flow) it is 10 s.
delta_t = 10; % (s)


%% =========== Step 3: Load experimental images =============

D = dir(fullfile(path_wte2,'Drop-UPW','*.tif'));
folder_name = D.folder;
image_names = {D(:).name};
upw_images = dtracker_create('imageset',folder_name,image_names);
N_frames = upw_images.n_frames;

%  Take a look to the images. Particles in the bulk of the flow move toward
%  the contact line, and particles on the drop surface move upwards due to
%  a thermal marangoni flow. Can you see this from the images already?
dtracker_show(upw_images)

%% =========== Step 3: Process images (testing) =============
%  The parameters for the processing are also contained in the MODEL
%  structure and depend from the method chosen and can be found under the
%  field 'model.processing'. In this example we show the main parameters,
%  parameters not shown here involve an advanced knowledge of the
%  algorithm, and in principle should not be modified. For a deeper
%  understanding of the algorithm used for method_1 we refer to
%  Rossi & Barnkob, Meas. Sci. Technol, 2020.
%
%  To track the particles, 'tracking_step' must be larger than 0. This
%  parameter indicates after how many frames we are searching the particles
%  to connect. In this case (tracking_step = 1), we connect particles in one
%  frame with particles to the next frame.
% 

model.processing.scaling_xyz = [scaling_x, scaling_y, scaling_z];
model.processing.cm_guess = .4;                % typical values between 0.4 and 0.5
model.processing.cm_final = .9;                % !OBS! cm_final must be larger than cm_guess 
model.processing.tracking_step = 1;           % > 0 to enable the tracking
model.processing.minmax_dx = [-30 30];        % THIS VALUES ARE ALWAYS PIXELS! 
model.processing.minmax_dy = [-30 30];        % THIS VALUES ARE ALWAYS PIXELS! 
model.processing.minmax_dz = [-0.12 0.12];    % THIS VALUES ARE NORMALIZED UNITS! 

%  Before running the evaluation, it is good practice to test the 
%  parameters on a few frames.   
n_frames_test = 1:3;
dat_upw = dtracker_process(model, upw_images, n_frames_test);
dtracker_show(upw_images,dat_upw)

%  DRILL: try to change some of the above parameters and repeat this cell 
%  (Ctrl+Enter). See how the number of detect particles change.

%% =========== Step 4: Process images (full processing) =============
%  Once you are satisfied with your parameters, you can run the full
%  processing.
dat_upw = dtracker_process(model, upw_images, 1:N_frames);
dtracker_show(upw_images,dat_upw)


%% =========== Step 5: Inspect your data =============
%  You can inspect you data using the function 'dtracker_show'. 
dtracker_show(dat_upw)

%  DRILL: The inspection panel is also a powerful tool for user validation
%  of data. Play around with the buttons, edit and scroll-down menus and
%  get an intuition about how to remove outliers. Remeber:
%  - When you draw a rectangle/polygon you need to double-click on it to 
%    confirm it.
%  - If you push 'Save', the changes are overwritten on your data, if you
%    push 'Save as' you have the option to create a new data.

%% =========== Step 6: Postprocessing: minimum track length =============
%  DefocusTracker has several functions for postprocessing data. In this
%  example we use the option 'min_n_id', that removes tracks with a length
%  less than a given number. 
min_length_of_track = 7;
dat_upw_post = dtracker_postprocess('min_n_id',dat_upw,min_length_of_track);

%% =========== Step 7: Plot your data! =============
%  CONGRATULATIONS! You have completed an evalution of a real experiment 
%  with DefocusTracker!
%
%  Running this cell will plot your results in a nice way, using the plot
%  capabilities of MATLAB. You can see tracks of particles with positive
%  velocities on the drop surface, moving upwards following a thermal 
%  Marangoni flow, and particles in the bulk of the fluid, moving toward
%  the contact line (the well known coffee-stain effect!). Further readings
%  on this interesting phenomenon can be found in:
%  Deegan et al. (1997) Nature, 389, 827-829,
%  Rossi et al. (2019) Physical Review E, 100(3), 033103.

figure, clf

vz_mean = zeros(1,max(dat_upw_post.id));
cmap = colormap('cool');
vz_cmap = linspace(-.8,.8,size(cmap,1));

for ii = 1:max(dat_upw_post.id)
    flag = dat_upw_post.id==ii;
    xt = dat_upw_post.x(flag);
    yt = dat_upw_post.y(flag);
    zt = dat_upw_post.z(flag);
    vz_mean(ii) = (zt(end)-zt(1))/sum(flag)/delta_t;
    [~, ind_vz] = min(abs(vz_cmap-vz_mean(ii)));
    plot3(xt,yt,zt,'.-','color',cmap(ind_vz,:)), hold on
end
xlabel('x (\mum)')
ylabel('y (\mum)')
zlabel('z (\mum)')

set(gca,'Clim',[-.8 .8])
hcb = colorbar('location','eastoutside');
set(hcb.Title,'String','v_z (\mum/s)')

axis([0 1600 0 1400 0 140])
view([70 30])
daspect([1 1 .33])
grid on

%% =========== Step 8: Correct for bias errors (advanced) =============
%  The 'method_1' uses one single particle image for the calibration.
%  This is done under the assumption that the particle images look the same
%  across the entire camera sensor. This approximation is not always true
%  and can generate significant bias error. This does not mean that 
%  measurement results obtained in Step 6 are wrong, but it means that the
%  uncertainty can be improved.
%
%  DefocusTraker currently does not have a package for bias correction yet,
%  however it is possible to use standard MATLAB functions to do this.  
%  
%  In this step we show an example of how bias errors can be corrected
%  using as reference the training images. To understand this step a deeper
%  understanding of MATLAB functions is required.
%

%% =========== Step 8.1: Process the training set =============
%  The training images contain particles across the entire image sensor 
%  and have the same 'true' z-coordinate in one frame. The idea is to use 
%  this information to create a correction function for the measurement.
%  The first step is to run a DefocusTracker processing, using the same
%  MODEL of the measurement. The only difference here is the
%  boundary of the tracking step
%

model2 = model;
model2.processing.tracking_step = 1;
model2.processing.minmax_dx = [-.5 .5];
model2.processing.minmax_dy = [-.5 .5];
model2.processing.minmax_dz = [-0.1 0.1];
data_train_test = dtracker_process(model2, images_train, 1:N_cal);

%% =========== Step 8.2: Create a fitting  =============
%  'data_train_test.z' are the 'measured' z-coordinates, but we can also 
%  calculate the 'true' z-coordinates using the frame number
%  'data_train_test.fr'.
%  We can then create a mapping function (2nd order polynomial function in
%  this case) to map the bias error in z as a function of x and y. 
%  For this type of measurement we do not expect bias error in the x and y
%  direction (as could happen for instance in case of perspective error
%  which is in this case negligible). For a deeper discussion of bias error
%  in defocusing measurement we refer to 
%    Barnkob & Rossi, Exp Fluids 61, 110 (2020).
%
%  NOTE: We calculate the correction on unscaled units. It is possible to
%  unscale (or scale) a dateset using 'dtracker_postprocessing'.
%

data_train_test = dtracker_postprocess('unscale',data_train_test);
X = data_train_test.x;
Y = data_train_test.y;    
Z = data_train_test.z;
Z0 = (data_train_test.fr-1)/(N_cal-1); % true z
Zerr = Z-Z0; % z-error
fit_poly22 = fittype( 'poly22' );
opts = fitoptions( 'Method', 'LinearLeastSquares','Robust','Bisquare' );
corr_fit_z = fit( [X(:), Y(:)], Zerr(:), fit_poly22, opts ); %

%  To obtain the corrected z-values, one must calculate the estimated error
Zerr_est = feval(corr_fit_z, [X(:), Y(:)]);
Zerr_est = reshape(Zerr_est,size(X));  % to be sure is a row vector
% subtract the error to the measured values
Zcorr = Z-Zerr_est;

figure, clf
subplot(1,2,1)
plot3(X,Y,Z,'.'), hold on
[Xg, Yg] = meshgrid(0:50:1200,0:50:1000);
Zg = feval(corr_fit_z, [Xg(:), Yg(:)]);
Zg = reshape(Zg,size(Xg));
mesh(Xg,Yg,Zg+0.5)
axis([0 1250 0 1050 0 1]), grid on
xlabel('x (pixel)'),  ylabel('y (pixel)'), zlabel('z/h')

subplot(1,2,2)
plot((Z0),Z,'.',Z0,Zcorr,'.')
xlim([0 1]), ylim([0 1]), grid on
xlabel('z/h - true'), ylabel('z/h - measured')
legend('uncorrected','corrected','location','NW')

disp(['Error without correction: ',...
    num2str(std(Z0-Z)*abs(scaling_z),'%0.3f\n'),' µm'])
disp(['Error after correction: ',...
    num2str(std(Z0-Zcorr)*abs(scaling_z),'%0.3f\n'),' µm'])

%  With the bias error correction, we reduced the depth error with 23 %!

%% =========== Step 8.3: Apply the correction to the data  =============
%  Remember that the correction is in unscaled units!
%

dat_upw_corr = dtracker_postprocess('unscale',dat_upw_post);
z_corr = feval(corr_fit_z, [dat_upw_corr.x(:), dat_upw_corr.y(:)]);
dat_upw_corr.z(:) = dat_upw_corr.z(:)-z_corr(:);
dat_upw_corr = dtracker_postprocess('scale',dat_upw_corr);

%% =========== Step 9: Remove outliers  =============
%  Another way to improve your measurement is to remove outliers, using
%  some knowledge of the measurement. In this case for instance, we know
%  that particles outside a certain radius are outliers therefore we remove
%  them. For more details about the experimental setup we refer to
%    Rossi & Barnkob, Meas. Sci. Technol, 2020.
%
%  You can use 'dtracker_postprocessing' to apply a flag to all the data in
%  the dataset.
%

drop_radius = 1200;
xr = drop_radius*cos(linspace(0,2*pi,100))+730;
yr = drop_radius*sin(linspace(0,2*pi,100))+1380;
flag = inpolygon(dat_upw_corr.x,dat_upw_corr.y,xr,yr);
dat_upw_corr = dtracker_postprocess('apply_flag',dat_upw_corr,flag);
dat_upw_corr = dtracker_postprocess('min_n_id',dat_upw_corr,7);

%% =========== Step 10: Plot your improved data! =============
%  CONGRATULATIONS! Now your measurement is much more accurate! Run this 
%  cell to plot again the data and see how they look!
%

figure, clf

vz_mean = zeros(1,max(dat_upw_corr.id));
cmap = colormap('cool');
vz_cmap = linspace(-.8,.8,size(cmap,1));

for ii = 1:max(dat_upw_corr.id)
    flag = dat_upw_corr.id==ii;
    xt = dat_upw_corr.x(flag);
    yt = dat_upw_corr.y(flag);
    zt = dat_upw_corr.z(flag);
    vz_mean(ii) = (zt(end)-zt(1))/sum(flag)/delta_t;
    [~, ind_vz] = min(abs(vz_cmap-vz_mean(ii)));
    plot3(xt,yt,zt,'.-','color',cmap(ind_vz,:)), hold on
end
xlabel('x (\mum)')
ylabel('y (\mum)')
zlabel('z (\mum)')

set(gca,'Clim',[-.8 .8])
hcb = colorbar('location','eastoutside');
set(hcb.Title,'String','v_z (\mum/s)')

axis([0 1600 0 1400 0 140])
view([70 30])
daspect([1 1 .33])
grid on


