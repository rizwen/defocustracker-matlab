%% DefocusTracker - Work Through Example 1 (WTE1)
%  Localizing the 3D position (x,y,z) of particles from synthetic images
%  www.defocustracking.com
%
%
%  Introduction
%  ------------
% 
%  DefocusTracker is a software for the 3D localization and tracking of the
%  positions of particles via their defocused particle images using the
%  General Defocusing Particle Tracking (GDPT) method.
%
%  You can find more information about DefocusTracker and GDPT on
%  https://defocustracking.com/
%
%  DefocusTracker is based on three data structures (MATLAB struct):  
%      IMAGESET, DATASET, MODEL,
%  and on five main functions: 
%      dtracker_create(), dtracker_show(), dtracker_train(),  
%      dtracker_process(), dtracker_postprocess(). 
%  You will familiarize with these elements throughout this tutorial.
%
%  This first example is based on synthetic images. You will learn how to 
%  create imagesets and calibration models and how to use them for
%  performing your first GDPT evaluation using DefocusTracker. 
%
%  Instruction
%  ------------
%
%  - Install DefocusTracker (see the README.md file)
%  - Download the 'Datasets-WTE1' from https://defocustracking.com/
%  - Follow the instructions and run this script cell-by-cell using the
%    command (Ctrl+Enter).
% 

%% Initialization
clc, clear, close all
%  Insert here the path where you copied the folder 'Datasets-WTE2'
%  If you copied it in the same folder where the 'Work_through_ex2.m' is
%  you can use '.\Datasets-WTE1'.
path_wte1 = '.\Datasets-WTE1';

%% =========== Step 1: Create an IMAGESET =============
%  For this example, you need to create two IMAGESETs. The first IMAGESET
%  contains training images stored in the folder 'Dataset-WTE1\Calib-noise0'.
%  The second IMAGESET contains the images to be analyzed and are stored in
%  the folder 'Dataset-WTE1\Data-overlapping-noise0-particles1100\'.
%
%  In DefocusTraker you create an IMAGESET with the function 
%  'dtracker_create()'.
%
%  You can create an IMAGESET interactively:
%  >> myimageset = dtracker_create('imageset');
%  Or you can decleare the folder and name of images as input parameters:
%  >> myimageset = dtracker_create('imageset', folder_name, image_names);
%

%  Create the IMAGESET for the training images
D = dir(fullfile(path_wte1,'Calib-noise0','*.tif'));
folder_name = D.folder;
image_names = {D(:).name};
training_images = dtracker_create('imageset',folder_name,image_names);

% Create the IMAGESET for the experimental images
D = dir(fullfile(path_wte1,'Data-overlapping-noise0-particles1100','*.tif'));
folder_name = D.folder;
image_names = {D(:).name};
noise0_p500_images = dtracker_create('imageset',folder_name,image_names);

%  DRILL: Create interactively the same IMAGESET using:
%  >> training_images = dtracker_create('imageset');

%  WARNING! If you get errors in loading images it might be that the path
%  of the images is wrong. You can correct that by loading again the images
%  using the interactive procedure (see DRILL above).

%% =========== Step 2: Inspect an IMAGESET =============
%  In DefocusTracker you can interactively look at data structures using
%  the function 'dtracker_show()'. 
%

%  Show the IMAGESET with the experimental images
dtracker_show(noise0_p500_images)

%  DRILL: Play with the buttons and text edits in the show window to look
%  at the experimental images.

%  TIP: You can output the n-frame in form of a matrix using:
%  >> myimage = dtracker_show(myimageset, n)

%% =========== Step 3: Create a MODEL (Method 1) =============
%  A DefocusTracker evaluation is based on a MODEL. A MODEL is a data 
%  structure that contains all the information for performing a GDPT
%  evaluation. The first step is to create a default MODEL data structure
%  using the 'dtracker_create()' function.
%
%  DefocusTracker has a modular architecture that allows for the
%  implementation of several calibration and evaluation models. In this
%  example we use 'method_1', but other methods can be used without change
%  of the workflow.
%

% Create MODEL based on Method 1:
model = dtracker_create('model','method_1');

%% =========== Step 4: Create a training set (Method 1) =============
%  To be able to use a model, we need to train it on a training set of
%  data. In DefocusTracker, a training set consists of:
%      - IMAGESET containing the training images.
%      - DATASET containing the coordinates of reference particle images
%        in the IMAGESET.
%
%  Using the 'method_1':
%      - The training IMAGESET must contain a sequence of particle images 
%        at different known z positions. Practically, each frame number 
%        corresponds to a not-scaled z position. 
%      - The training DATASET must contain the coordinates of only ONE 
%        particle per frame.
%
%  For more information about the "method_1", we refer to
%       Barnkob & Rossi, Exp. Fluids 61, 110 (2020)
%       Rossi & Barnkob, Meas. Sci. Technol, 2020
%
%  You can initialize a DATASET using the function 'dtracker_create()' 
%  >> mydataset = dtracker_create('dataset')
%  'mydataset' will have all fields empty.
%
%  >> mydataset = dtracker_create('dataset',n)
%  'mydataset' will have all fields with arrays of zeros(1,n).
%

%  In the current example, in the training IMAGESET we have a sequence of 
%  501 images, but we select a sequence with a stride of 10 frames.
selected_frames = 1:10:501;
N_cal = length(selected_frames);

%  Initialize the DATASET
training_data = dtracker_create('dataset',N_cal);

%  Center coordinate of the calibration particle image
training_data.x = zeros(1,N_cal) + 30;
training_data.y = zeros(1,N_cal) + 30;

%  By convention, in GDPT analyses it useful to set the unscaled z 
%  coordinates in terms of values normalized with the total measurement 
%  height. The value will be between 0 and 1.
training_data.z = linspace(0,1,N_cal);
training_data.fr = selected_frames;

%  You can check the training set using dtracker_show(). You will see a 
%  cross in the center of the particle images considered in the calibration.
dtracker_show(training_images,training_data)

%% =========== Step 5: Train a MODEL (Method 1) =============
%  We can train the MODEL using the training set. The parameters used for
%  the training are given in the structure:
%      model.training
%
%  In this example we will keep all the default values. We only modify
%  the size (imwidth x imheight) of the calibration images. This size
%  should contain the largest defocused particle image. More parameters
%  will be discussed in the WTE2.
%

%  Change the training parameters and train the MODEL
model.training.imwidth = 51;
model.training.imheight = 51;
model = dtracker_train(model,training_images,training_data);

%  Show the MODEL 
dtracker_show(model)

%  You can look at the calibration images in the bottom left panels. The 
%  other panels contain more technical information about the calibration 
%  but can also be ignored. For more information we refer to 
%  Barnkob & Rossi, Exp Fluids 61, 110 (2020). 


%% =========== Step 6: Process an IMAGESET (Method 1)  =============
%  We are now ready to run our evaluation using the trained MODEL on an
%  imageset. In DefocusTracker, an evaluation is performed using the 
%  function 'dtracker_process()':
%
%  >> mydataset = dtracker_process(model, myimageset, myframes)
%
%  The required inputs are: a trained MODEL, an IMAGESET with the images
%  to be processed, and an array with integer indices of the images to be
%  processed. The resulting output is a DefocusTracker DATASET.
%
%  The parameters for the evaluation are given in the structure:
%      model.processing
%  
%  In this example we used all standard values, we only change two
%  parameters, related to the guess and final cm. In 'method_1', cm is a
%  pameter (between 0 and 1) that evaluates how well a target particle
%  matches with a given calibration particle in the model (with 1 perfect
%  match). In particular, low cm detects more particles but with larger
%  uncertainty and vice versa. In 'method_1' you can set two cm values, one
%  for the initial guessing procedure, and one for the final evaluation.
%  
%  For more details, we refer to Rossi & Barnkob, MST, 2020.
%

%  Set the processing parameters
model.processing.cm_guess = .4;
model.processing.cm_final = .5;

%  Process all images in the "noise0_p500_images" IMAGESET
N_frames = noise0_p500_images.n_frames;
noise0_p500_data = dtracker_process(model, noise0_p500_images, 1:N_frames);

%  Display the DATASET resulting from the processing
dtracker_show(noise0_p500_data,'plot_3d')

%  CONGRATULATIONS! You have now successfully performed a GDPT evaluation
%  using DefocusTracker
%
%  DRILL: You can display and filter your processed data using
%  >> dtracker_show(noise0_p500_data)
%
%  NOTE: You may notice that the evaluation is not the same everywhere, but  
%  there is a small band where less particles are detected. This is due to 
%  a particle image shape which is harder to detect in that region. For a
%  deeper discussion on these aspects we refer to 
%  Barnkob & Rossi, Exp Fluids 61, 110 (2020).

%% ========= Step 7 (optional): Check the error on true DATASET ===========
%  In this case, since we used synthetic images, we know the true values 
%  and we can calculate the error in our measurement. More details on how
%  the error is estimated are given in 
%  Barnkob & Rossi, Exp Fluids 61, 110 (2020). 
%
%  You can run this cell to see if you get the expected uncertainty. This 
%  dataset corresponds approximately to the case:
%      - Overlapping particle images, no noise, Ns = 0.545,
%  in Barnkob & Rossi, Exp Fluids 61, 110 (2020). 

noise0_p500_true = dtracker_create('import_ascii',...
    fullfile(path_wte1,'true_coordinates.txt'));

par.outliers_x = 2;
par.outliers_y = 2;
par.outliers_z = 0.1;
par.delta_points = 20;
[errors, errors_delta] = dtracker_postprocess('compare_true_values',...
   noise0_p500_data,noise0_p500_true,par);

disp('          ')
disp('------ Error analysis ------')
disp(['Error for x (pixels) = ',num2str(errors.sigma_x,'%0.3f')])
disp(['Error for y (pixels) = ',num2str(errors.sigma_y,'%0.3f')])
disp(['Error for z/h = ',num2str(errors.sigma_z,'%0.3f')])
disp(['Recall = ',num2str(errors.recall,'%0.3f')])
disp('------ Expected values ------')
disp('Error for x (pixels) = 0.440')
disp('Error for y (pixels) = 0.443')
disp('Error for z/h = 0.020')
disp('Recall = 0.330')

%  NOTE: Recall is the ratio between (true positive)/(true positive + false 
%  negative). In this case we have:
%  True positive = valid detected particles
%  False negative = undetected particles
%  We have another category that are rejected detected particles. These can
%  be considered as false positive and are not considered in calculating
%  the recall.

%  DRILL: Try to change the parameters of the processing (cm_guess and
%  cm_final) and see how the errors and recall change.
